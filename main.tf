# Creating VPC
resource "aws_vpc" "test_vpc" {
  cidr_block           = var.vpc_cidr
  instance_tenancy     = "default"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "test_vpc"
  }
}



# Creating public subnet
resource "aws_subnet" "public_subnet" {
  vpc_id            = aws_vpc.test_vpc.id
  cidr_block        = var.public_subnet_cidr
  availability_zone = var.AZ_2a
  

  tags = {
    Name = "public_subnet"
  }
}

# Creating private subnet
resource "aws_subnet" "private_subnet" {
  vpc_id            = aws_vpc.test_vpc.id
  cidr_block        = var.private_subnet_cidr
  availability_zone = var.AZ_2b
  

  tags = {
    Name = "private_subnet"
  }
}


# Public route table
resource "aws_route_table" "public_route_table" {
  vpc_id = aws_vpc.test_vpc.id

  tags = {
    Name = "public_route_table"
  }
}

# Private route table
resource "aws_route_table" "private_route_table" {
  vpc_id = aws_vpc.test_vpc.id

  tags = {
    Name = "private_route_table"
  }
}

# Public route table association to public subnet
resource "aws_route_table_association" "public_route_table_assoc" {
  subnet_id      = aws_subnet.public_subnet.id
  route_table_id = aws_route_table.public_route_table.id
}

# Private route table association to the private subnet
resource "aws_route_table_association" "private_route_table_assoc" {
  subnet_id      = aws_subnet.private_subnet.id
  route_table_id = aws_route_table.private_route_table.id
}

# Internet gateway (IGW)
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.test_vpc.id

  tags = {
    Name = "igw"
  }
}

# Associating igw to the public route table (** Using aws Terraform route resource)
resource "aws_route" "public_route_table_assoc" {
  route_table_id         = aws_route_table.public_route_table.id
  gateway_id             = aws_internet_gateway.igw.id
  destination_cidr_block = var.public_route_table_assoc_cidr
}

# Nat_gw in the public subnet
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = aws_eip.eip_assoc.id
  subnet_id     = aws_subnet.public_subnet.id

  tags = {
    Name = "nat_gw"
  }

  # To ensure proper ordering, it is recommended to add an explicit dependency
  # on the Internet Gateway for the VPC.
  depends_on = [aws_internet_gateway.igw]
}

# Elastic_ip
resource "aws_eip" "eip_assoc" {
  vpc      = true
}

# Editing private route table to take nat_gw
resource "aws_route" "private_route_table_edit" {
  route_table_id         = aws_route_table.private_route_table.id
  gateway_id             = aws_nat_gateway.nat_gw.id
  destination_cidr_block = var.nat_gw_route
}




