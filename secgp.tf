# Security group
resource "aws_security_group" "Secgrp" {
  name        = "allow_HTTP"
  description = "Allow TCP inbound traffic"
  vpc_id      = aws_vpc.test_vpc.id

  ingress {
    description      = "Opened ingress HTTP port"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = [var.http_cidr] #this is a list

      }

  ingress {
    description      = "Opened ingress SSH port"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = [var.ssh_cidr]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    
  }

  tags = {
    Name = "allow_tcp"
  }
}