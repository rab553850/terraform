# Setting vpc_cidr as variable
variable "vpc_cidr" {
  type        = string
  description = "name of vpc cidr"
  default     = "10.0.0.0/16"

}

# Setting public subnet cidr as variable
variable "public_subnet_cidr" {
  type        = string
  description = "name of public subnet cidr"
  default     = "10.0.1.0/24"
}

# Setting private subnet cidr as variable
variable "private_subnet_cidr" {
  type        = string
  description = "name of private subnet cidr"
  default     = "10.0.2.0/24"
}




# Setting AZ_2a as variable
variable "AZ_2a" {
  type        = string
  description = "name of availability zone 2a"
  default     = "us-west-2a"

}

# Setting AZ_2b as variable
variable "AZ_2b" {
  type        = string
  description = "name of availability zone 2b"
  default     = "us-west-2b"

}

# Setting Region as variable
variable "region_name" {
  type        = string
  description = "name of region"
  default     = "us-west-2"
}

# Setting "aws_route" "public_route_table_assoc" cidr as variable
variable "public_route_table_assoc_cidr" {
  type        = string
  description = "pubiic_route_table_assoc_cidr"
  default     = "0.0.0.0/0"

}


# Setting Secgrp Ingress http_cidr as variable
variable "http_cidr" {
  type        = string
  description = "Ingress HTTP cidr"
  default     = "0.0.0.0/0"
  }

# Setting Secgrp Ingress ssh_cidr as variable
variable "ssh_cidr" {
  type        = string
  description = "Ingress SSH cidr"
  default     = "0.0.0.0/0"
}

# Setting private_route_table_nat_gw _route as Variable
variable "nat_gw_route" {
  type        = string
  description = "nat_gw cidr"
  default     = "0.0.0.0/0"
}


